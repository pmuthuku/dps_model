#!/bin/bash
#Argument 1: Track file 2: outputfile

#$ESTDIR/bin/ch_track $1 -otype est_ascii | sed -n '306,$ p' > data_cding
FILENM=`basename $1 .wts`

perl -ane '
for($i=279; $i <= 292; $i++){
print "$F[$i] ";
}
print "\n";
' < $1 > lpc_part

perl -ane '
for($i=20; $i <=277; $i++){
print "$F[$i] ";
}
print "\n";
' < $1 > pca_part

#$ESTDIR/bin/ch_track $1 -otype est_ascii | sed '25,300 d' | perl -ane '
#if($F[0] eq "NumChannels"){
#  $F[1]=17;
#}
#@x=@F[0..19]; $num=@x;
#for($i=0; $i < $num; $i++){
#  if($i!=2){
#    print "$x[$i] "; 
#  }
#}
#print "\n";
#' > speech_lpc_file

awk '{print $1}' < $1 > tim1
paste tim1 lpc_part > stoc_part
paste tim1 pca_part > determ_part

$DPS/rewrite/decoder_pca -i determ_part -o determf -s 256 -e ../ps_correct_eigs
$DPS/rewrite/lpc_pm.sh stoc_part 12 stocf

#$DPS/normalizer determf out1
#$DPS/normalizer stocf out2

$DPS/upperlowercombiner -i determf -j stocf -o out -w 0.86
#awk '{print $1*3000}' < out > tesout
$ESTDIR/bin/ch_wave out -itype raw -istype ascii -scale 0.2 -f 16000 -otype riff -o res.wav
$LPC/lpc_resynth -res res.wav -order 16 ../lpc/$FILENM.lpc temp.wav
$ESTDIR/bin/ch_wave temp.wav -scale 3 -o $2
rm lpc_part determ_part tim1 stoc_part stocf determf out res.wav temp.wav
