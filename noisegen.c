#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(int argc,char **argv){
    int j;
    int length=atoi(argv[1]);
    FILE *fp1;
    char *outputfile=argv[2];
    float scale=atof(argv[3]);
    srand(time(NULL));
    if(argc < 2){
	printf("Invalid number of arguments.\n Usage:\n 1) Length\n 2) Outputfile\n 3) Scale\n ");
	exit(1);
    }
    fp1=fopen(outputfile,"w");
    for(j=0;j < length;j++){
	fprintf(fp1,"%f\n",scale*(((float)rand()/(float)RAND_MAX)-0.5));
    }
    fclose(fp1);
    return 0;
}
