#!/bin/bash
# Usage 1) Residual
# Must only be run in voice directory

max=`perl -ane '
@x=split(/=/,$F[0]);
print "$x[1] "' < etc/f0.params | awk '{print $3}'`

min=`perl -ane '                                                              
@x=split(/=/,$F[0]);                                                          
print "$x[1] "' < etc/f0.params | awk '{print $4}'`

cd lpc/
for i in *.res
do
FILNM=`basename $i .res`
echo $FILNM

$ESTDIR/bin/sigfilter $i -o u4k -hpfilter 4000 -forder 55 -double
$ESTDIR/bin/sigfilter $i -otype raw -ostype ascii -o l4k -lpfilter 4000 -forder 55 -double

#Deterministic part
sed '1,/EST_Header_End/ d' ../pm/$FILNM.pm | awk '{print int($1*16000)}' > data.pm
/home/pmuthuku/code/dps_model/coder_pca -i l4k -o l4k.pcwts -p data.pm -s 256 -e ../eigs.txt -m $max -n $min

#Stochastic part
$ESTDIR/bin/sig2fv u4k -coefs "lpc" -f 16000 -lpc_order 12 -o testa -preemph 0.95 -factor 3 -window_type hamming -pm ../pm/$FILNM.pm -otype est
sed '1,/EST_Header_End/ d' < testa > u4k.lpc 

paste l4k.pcwts u4k.lpc > $FILNM.wts
rm l4k u4k data.pm testa l4k.pcwts u4k.lpc
done