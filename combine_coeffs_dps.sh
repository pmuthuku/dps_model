#!/bin/bash

if [ ! -d ccoefs ]
then
    mkdir ccoefs
fi

if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi

CG_TMP=cg_tmp_$$

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do
    fname=$i
    echo $fname "COMBINE_COEFFS_PSYNC (f0,lpcs,dps_features,v)"

     # need to get time in there and smooth F0 at each pitch mark
    $ESTDIR/bin/ch_track -otype ascii f0/$fname.f0 |
    awk '{if (NR == 1) { print $1; print $1;} print $1}' |
    awk 'BEGIN {printf("{\n");}
           {printf("f0[%d]=%f;\n",NR,$1);}
           END {printf("}\n")}' >$CG_TMP.awk.f0
    echo '{ printf("%f 1 %f\n",$1,f0[int($1/0.005)]); }' >>$CG_TMP.awk.f0
    
    $ESTDIR/bin/ch_track -otype est_ascii lpc/$fname.lpc | sed '1,/EST_Header_End/ d' > $CG_TMP.lpc
    
    awk -f $CG_TMP.awk.f0 $CG_TMP.lpc >$CG_TMP.time



    # Doing the same with voicing
    cat v/$fname.v |
    awk '{if (NR == 1) { print $1; print $1;} print $1}' |
    awk 'BEGIN {printf("{\n");} 
           {printf("f0[%d]=%f;\n",NR,$1);}
           END {printf("}\n")}' >$CG_TMP.awk.v
    echo '{ printf("%f\n",f0[int($1/0.005)]); }' >>$CG_TMP.awk.v

    awk -f $CG_TMP.awk.v $CG_TMP.lpc > $CG_TMP.v


    $ESTDIR/bin/ch_track -otype ascii lpc/$fname.lpc > $CG_TMP.lpc

    paste $CG_TMP.time $CG_TMP.lpc lpc/$fname.wts $CG_TMP.v |
    awk '{if (l==0) 
              l=NF;
            else if (l == NF)
              print $0}' >$CG_TMP.all
    echo EST_File Track >$CG_TMP.est
    echo DataType ascii >>$CG_TMP.est
    echo NumFrames `cat $CG_TMP.all | wc -l` >>$CG_TMP.est
    echo NumChannels `head -1 $CG_TMP.all | awk '{print NF-2}'` >>$CG_TMP.est
    echo NumAuxChannels 0 >>$CG_TMP.est
    echo EqualSpace 0 >>$CG_TMP.est
    echo BreaksPresent true >>$CG_TMP.est
    head -1 $CG_TMP.all |
    awk '{for (i=1; i<=NF-2; i++)
             printf("Channel_%d track_%d\n",i-1,i-1)}' >>$CG_TMP.est
    echo ByteOrder 01 >>$CG_TMP.est
    echo "CommentChar ;" >>$CG_TMP.est
    echo file_type 13 >>$CG_TMP.est
    echo name $fname.ccoeffs >>$CG_TMP.est
    echo EST_Header_End >>$CG_TMP.est
    cat $CG_TMP.all >>$CG_TMP.est

    $ESTDIR/bin/ch_track -otype est_binary $CG_TMP.est -o ccoefs/$fname.mcep

    rm -f $CG_TMP.*

done