#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc,char **argv){
  FILE *ip1,*ip2,*out;
  char *inp1,*inp2,*output;
  int c;
  float data1[150000],data2[150000];
  long int len1=0,len2=0;
  float x;
  float weight;
  int i,size;
  float max1=0,max2=0;
  while((c=getopt (argc,argv,"i:j:o:w:")) != -1)
    switch(c){
    case 'i':
      inp1=optarg;
      break;
    case 'j':
      inp2=optarg;
      break;
    case 'o':
    output=optarg;
      break;
    case 'w':
      weight=atof(optarg);
      break;
    }
  ip1=fopen(inp1,"r");
  //printf("\n\nweight=%f\n\n",weight);
  while(!feof(ip1)){
    fscanf(ip1,"%f",&data1[len1++]);
    if(data1[len1-1] > max1){
      max1=data1[len1-1];
    }
  }
  fclose(ip1);
  ip2=fopen(inp2,"r");
  while(!feof(ip2)){
    fscanf(ip2,"%f",&data2[len2++]);
    if(data2[len2-1] > max2){
      max2=data2[len2-1];
    }
  }
  fclose(ip2);
  size=(len1 > len2) ? len1 : len2;
  out=fopen(output,"w");
  for(i=0; i < size -2; i++){
    if(i >= len1)
      data1[i]=0;
    if(i >= len2)
      data2[i]=0;
//    data1[i]=data1[i]/max1;
//    data2[i]=data2[i]/max2;
   x=data1[i]+data2[i];
//  printf("%f\n",x);
    fprintf(out,"%f\n",x);
  }
  fclose(out);
  return 0;
}
