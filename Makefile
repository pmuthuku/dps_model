
SCMFILES = 
SCRIPTS = coder_decoder.sh generic_decoder.sh lpc_pm.sh my_decoder.sh \
	pca_data_extractor.sh tearer.sh voice_coder.sh
SRCS = coder_nopca.c decoder_pca.c coder_pca.c stoc_res_res_coder.c \
	decoder_nopca.c lpc_pm_resynth.c stoc_res_res_decoder.c
FILES = Makefile $(SCMFILES) $(SCRIPTS) $(SRCS)

all : decoder_pca lpc_pm_resynth noisegen normalizer upperlowercombiner \
	coder_pca coder_nopca decoder_nopca

CC = gcc
CFLAGS = -g

coder_nopca: coder_nopca.o
	$(CC) $(CFLAGS) -o $@ coder_nopca.o -lm
coder_pca: coder_pca.o
	$(CC) $(CFLAGS) -o $@ coder_pca.o -lm
decoder_nopca: decoder_nopca.o
	$(CC) $(CFLAGS) -o $@ decoder_nopca.o -lm
decoder_pca: decoder_pca.o
	$(CC) $(CFLAGS) -o $@ decoder_pca.o -lm
lpc_pm_resynth: lpc_pm_resynth.o
	$(CC) $(CFLAGS) -o $@ lpc_pm_resynth.o -lm
stoc_res_res_coder: stoc_res_res_coder.o
	$(CC) $(CFLAGS) -o $@ stoc_res_res_coder.o -lm
stoc_res_res_decoder: stoc_res_res_decoder.o
	$(CC) $(CFLAGS) -o $@ stoc_res_res_decoder.o -lm
noisegen: noisegen.o
	$(CC) $(CFLAGS) -o $@ noisegen.o -lm
normalizer: normalizer.o
	$(CC) $(CFLAGS) -o $@ normalizer.o -lm
upperlowercombiner: upperlowercombiner.o
	$(CC) $(CFLAGS) -o $@ upperlowercombiner.o -lm


