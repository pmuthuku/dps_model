clear; clc;

d = load('ps_traindata');
c = d'*d; %Calculating correlation matrix (subtract the mean?)
[v,~] = eig(c); 
v = fliplr(v);
v = v';
save('eigs.txt','v','-ascii');