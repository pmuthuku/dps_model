#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

//Fast algorithm to calculate LCM
int leastcommonmultiple(int k1,int k2){
  int x1=k1,x2=k2;
  while(x1!=x2){
    if(x1 < x2)
      x1=x1+k1;
    else if (x2 < x1)
      x2=x2+k2;
  }
  return x1; //Or x2
}


int main(int argc,char **argv){
  FILE *fp1,*fp2;
  int i;
  int c,j,k;
  int pm_locs[1000];
  float amplitude[1000];
  float framelength[1000];
  float excit[150000]={0};
  long int last=0;
  int length=0;
  int scale1;
  int nntemp;
  char *inputfile,*outputfile;
  float p0,p1,p2,p3;
  float q0,q1,q2,q3;
  float t;
  float tens=0.2;
  float time;
  int lcm,m;
  int sizef=256;
  float samplingrate=16000;
  while((c=getopt(argc,argv,"i:o:s:")) != -1)
    switch(c){
    case 'i':
      inputfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 's':
      sizef=atoi(optarg);
      break;
    }
  float pca_wts[1000][sizef];//Just called pca wts for convention. Not actually pca wts
  float frame[sizef];
  float temp[sizef*400];

  //Reading in data
  fp1=fopen(inputfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%f",&time);
    pm_locs[length]=time*samplingrate;
    fscanf(fp1,"%f",&framelength[length]);
    fscanf(fp1,"%f",&amplitude[length]);
    for(i=0; i < sizef; i++)
      fscanf(fp1,"%f",&pca_wts[length][i]);
    length++;
  }

  //Actual decoding part
  for(i=0; i < length-2; i++){

    //Adjusting amplitude
    for(j=0; j < sizef; j++)
      frame[j]=amplitude[i]*pca_wts[i][j];

    //Resampling
    if(framelength[i]==sizef){
      for(j=pm_locs[i]-(framelength[i]/2)/*verify*/,k=0; k < framelength[i]; j++,k++){
	excit[j]=excit[j]+frame[k];
	last=j;
      }
    }
    if(framelength[i]!=sizef){
      nntemp=framelength[i];
      lcm=leastcommonmultiple(nntemp,sizef);
      scale1=lcm/sizef;
      //Upsampling first
      for(j=0,k=0; j < lcm; j=j+scale1,k++){
	temp[j]=frame[k];
        //Boundary conditions
        p1=frame[k];
        if(k < sizef-1){
          p2=frame[k+1];
        }
        else{
          p2=0;
        }
        if(k > 0){
          p0=frame[k-1];
        }
        else{
          p0=0;
        }
        if(k >= (sizef-3)){
          p3=0;
        }
        else{
          p3=frame[k+2];
        }
        //Variables for the spline polynomial
        q0=p1;
        q1=tens*(p2-p0);
        q2=3*(p2-p1)-tens*(p3-p1)-2*tens*(p2-p0);
        q3=(-2)*(p2-p1)+tens*(p3-p1)+tens*(p2-p0);
        for(m=1; m <= (scale1-1); m++){
          //Catmull-Rom Spline interpolation to upsample signal
          t=((float)m)/((float)scale1);
          temp[j+m]=(q0+q1*t+q2*t*t+q3*t*t*t);
        }
      }
      //Now downsampling
      scale1=lcm/nntemp;
      for(j=(pm_locs[i]-(framelength[i]/2))/*verify*/,k=0; j < (pm_locs[i]+(framelength[i]/2)); j++,k=k+scale1){
	  excit[j]=excit[j]+temp[k];
	  last=j;
	}

    }//End of resampling
  }//End of decoding

  fp2=fopen(outputfile,"w");
  for(i=0; i <= last; i++)
    fprintf(fp2,"%f\n",excit[i]);
  fclose(fp2);
  return 0;
}
