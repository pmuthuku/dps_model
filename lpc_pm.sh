#!/bin/bash
# Usage: 1) Lpc coefs file  2) Order 3) Output file

no_frames=`wc -l $1 | awk '{print $1}'`
res_len=`sed -n '$,$ p' < $1 | awk '{print int($1*16000)}'`
/home/pmuthuku/code/dps_model/noisegen $res_len noise 5
/home/pmuthuku/code/dps_model/lpc_pm_resynth -f $1 -r noise -o $3 -p $2 -n $no_frames -l $res_len
rm noise
