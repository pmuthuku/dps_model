#!/bin/bash

sed -n '253,$ p' < $1 > tes
FILENM=`basename $1 .mcep`

perl -ane '
$time=$F[0]+0.005;
print "$time\n";
' < tes > tim1

perl -ane '
for($i=206;$i < 234; $i=$i+2){
  print "$F[$i] ";
}
print "\n";
' < tes > stoc1

perl -ane '
for($i=40; $i < 204; $i=$i+2){
  print "$F[$i] ";
}
print "\n";
' < tes > determ1

paste tim1 determ1 tim1 stoc1 > $FILENM.wts_synth
rm tim1 determ1 stoc1
