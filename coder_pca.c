#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

//Fast algorithm to calculate LCM
int leastcommonmultiple(int k1,int k2){
  int x1=k1,x2=k2;
  while(x1!=x2){
    if(x1 < x2)
      x1=x1+k1;
    else if (x2 < x1)
      x2=x2+k2;
  }
  return x1; //or x2
}

int main(int argc,char **argv){
  FILE *fp1,*fp2;
//  FILE *fp3;
  int c;
  float excit[150000];
  int pm_locs[1000];// Could also be glottal closure instances
  long int len_ex=0,len_pm=0;
  int i,j,k,m;
  float abspeak=0;
  char *inputfile,*outputfile,*pmfile,*eigsfile,*meanfile;
  int halfwindsize;
  float scale;
  float nntemp;
  int lcm;
  int sizef=256;
  int no_pts;
  float p0,p1,p2,p3;
  float q0,q1,q2,q3;
  float t;
  float tens=0.2;
  int maxf0,minf0;
  int minframesize,maxframesize;
  while((c=getopt(argc,argv,"i:o:p:s:e:m:n:")) != -1)
    switch(c){
    case 'i':
      inputfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 'p':
      pmfile=optarg;
      break;
    case 's':
      sizef=atoi(optarg);
      break;
    case 'e':
      eigsfile=optarg;
      break;
    case 'm':
      maxf0=atoi(optarg);
      break;
    case 'n':
      minf0=atoi(optarg);
      break;
    }
  int no_vectors=sizef;
  float frame[810];
  int framesize=0;
  float output[no_vectors];
  float temp[sizef*800];
  int tempsize=0;
  float blackman[sizef];
  float pi=3.141592;
  float mean[sizef];
  float eigs[no_vectors][sizef];
  float eig_strs[sizef];
  float tempframe[sizef];
  int samplingrate=16000;
  minframesize=samplingrate/maxf0; //Not a mistake min->max
  maxframesize=samplingrate/minf0;

  //generating Blackman window here
  for(i=0; i < sizef; i++){
    blackman[i]=0.42-0.5*cos((2*pi*i)/sizef)+0.08*cos((4*pi*i)/sizef);
  }

  //Excitation file
  fp1=fopen(inputfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%f",&excit[len_ex++]);
  }
  fclose(fp1);

  //Pitch marks locations (in samples)
  fp1=fopen(pmfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%d",&pm_locs[len_pm++]);
  }
  fclose(fp1);

  //Eigs file input
  fp1=fopen(eigsfile,"r");
  for(i=0; i < no_vectors; i++){
    for(j=0; j < sizef; j++){
      /*if(i==0)
        fscanf(fp1,"%f",&eig_strs[j]);
	else*/
        fscanf(fp1,"%f",&eigs[i][j]);
    }
  }
  fclose(fp1);

  //Mean file
  /*  fp1=fopen(meanfile,"r");
  for(i=0; i < sizef; i++){
    fscanf(fp1,"%f",&mean[i]);
  }
  fclose(fp1);*/

  //Coding starts here
  fp2=fopen(outputfile,"w");
  //fp3=fopen("debug1","w");
  for(i=0; i < len_pm -1; i++){// <--------------------------
    //Finding frame size                                    |
    //Account for boundary conditions first         //      |
    if(i==0){                                       //      |
      halfwindsize=pm_locs[1]-pm_locs[0];           //      |
    }                                               //      |
    else if(i==len_pm-2){ // these two are linked -----------
      halfwindsize=pm_locs[len_pm-2]-pm_locs[len_pm-3];
    }
    else if((pm_locs[i]-pm_locs[i-1]) < (pm_locs[i+1]-pm_locs[i])){
      halfwindsize=pm_locs[i]-pm_locs[i-1];
    }
    else{
      halfwindsize=pm_locs[i+1]-pm_locs[i];
    }

    //To make sure that frames are not too small (too high pitch)
    if(halfwindsize < minframesize)
      halfwindsize=minframesize; //54 is for the rms voice
    //To make sure that frames are not too big (too low pitch)
    if(halfwindsize > maxframesize)
      halfwindsize=maxframesize; //400 is for the rms voice

    framesize=0;
    for(j=0,k=pm_locs[i]-halfwindsize; k <= pm_locs[i]+halfwindsize; j++,k++){
      frame[j]=excit[k];
      framesize=j+1;
    }

    //Now for resampling
    if(framesize==sizef){
      for(k=0; k < sizef; k++){
        output[k]=frame[k];
      }
    }
    if(framesize != sizef){
      //Resampling part 1: Upsampling
      nntemp=sizef+1;
      lcm=leastcommonmultiple(framesize,nntemp);
      scale=lcm/framesize;
      no_pts=scale-1;
      for(j=0,k=0; j < lcm; j=j+scale,k++){
        temp[j]=frame[k];
        //Boundary conditions for points in spline
        p1=frame[k];
        if(k < framesize){
          p2=frame[k+1];
        }
        else{
          p2=0;
        }
        if(k > 0){
          p0=frame[k-1];
        }
        else{
          p0=0;
        }
        if(k >= (framesize-2)){
          p3=0;
        }
        else{
          p3=frame[k+2];
        }

        //Variables for the spline polynomial
        q0=p1;
        q1=tens*(p2-p0);
        q2=3*(p2-p1)-tens*(p3-p1)-2*tens*(p2-p0);
        q3=(-2)*(p2-p1)+tens*(p3-p1)+tens*(p2-p0);
        for(m=1; m <= no_pts; m++){
          //Catmull-Rom Spline interpolation to upsample signal
          t=((float)m)/((float)no_pts+1);
          temp[j+m]=(q0+q1*t+q2*t*t+q3*t*t*t);
        }
      }
      tempsize=j;
      //Now downsampling
      scale=lcm/nntemp;
      no_pts=scale-1;
      abspeak=0;
      for(j=0,k=0; j < sizef; j++,k=k+scale){
        tempframe[j]=temp[k];
        if(fabsf(tempframe[j]) > abspeak)
          abspeak=fabsf(tempframe[j]);
      }
    }

    if(abspeak==0)
      abspeak=1;
    fprintf(fp2,"%f\t%d\t%f\t",((float)pm_locs[i]/16000),framesize,abspeak);

    //Coding using Eigen vectors
    //Normalization and Mean subtraction
    for(j=0; j < sizef; j++){
      tempframe[j]=tempframe[j]/abspeak;
      // tempframe[j]=tempframe[j]-mean[j];//Is this order correct?
    }

    for(j=0; j < sizef; j++)
      output[j]=0;

    //Debug stuff                                                             
    /*for(j=0; j < sizef; j++){
      fprintf(fp3,"%f ",tempframe[j]);
    }
    fprintf(fp3,"\n");*/


    //PCA coding
    for(j=0; j < no_vectors; j++){
      for(k=0; k < sizef; k++){
	output[j]=output[j]+eigs[j][k]*tempframe[k];
      }
    }

    //Writing Output
    for(j=0; j < no_vectors; j++)
      fprintf(fp2,"%f ", output[j]);

    fprintf(fp2,"\n");
  }//End of coding
  fclose(fp2);

  return 0;
}
