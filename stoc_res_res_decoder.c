#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc,char **argv){
  FILE *fp1,*fp2;
  int c;
  int i,j;
  char *noisefile,*ampfile,*outputfile,*pmfile;
  int len_pm=0;
  int pm_locs[1000];
  float noisesample;
  float x;
  while((c=getopt(argc,argv,"n:a:o:p:")) != -1)
    switch(c){
    case 'n':
      noisefile=optarg;
      break;
    case 'a':
      ampfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 'p':
      pmfile=optarg;
      break;
    }

  //Reading in pitchmarks
  fp1=fopen(pmfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%d",&pm_locs[len_pm++]);
  }
  fclose(fp1);

  float amps[len_pm];
  i=0;
  fp1=fopen(ampfile,"r");
  //Reading in amplitudes
  while(!feof(fp1)){
    fscanf(fp1,"%f",&amps[i++]);
  }
  fclose(fp1);

  //Reading in noise file
  fp1=fopen(noisefile,"r");
  fp2=fopen(outputfile,"w");
  i=0;j=0;
  while(!feof(fp1)){
    fscanf(fp1,"%f",&noisesample);
    x=noisesample*amps[i];
    j++;
    if(j >= pm_locs[i]){
      i++;
    }
    fprintf(fp2,"%f\n",x);
  }
  fclose(fp1);
  fclose(fp2);
  return 0;
}
