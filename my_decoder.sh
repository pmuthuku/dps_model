#!/bin/bash

echo "Check if the eigs are in the right place !!!"

perl -ane '
for($i=0; $i < 259; $i++){
print "$F[$i] ";
}
print "\n";' < $1 > determ

perl -ane '                                           
for($i=259; $i < 274; $i++){
print "$F[$i] ";
}
print "\n";' < $1 > stoc

/home/pmuthuku/code/dps_model/decoder_pca -i determ -o determf -s 256 -e ../eigs.txt
/home/pmuthuku/code/dps_model/lpc_pm.sh stoc 12 stocf

/home/pmuthuku/code/dps_model/upperlowercombiner -i determf -j stocf -o out -w 0.86
$ESTDIR/bin/ch_wave out -itype raw -istype ascii -scale 0.2 -f 16000 -otype riff -o res1.wav
rm determ determf stoc stocf out
