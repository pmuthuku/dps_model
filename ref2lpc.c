#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

int main(int argc,char **argv){
  FILE *input,*output;
  int c;
  char *inputfile,*outputfile;
  int order;
  int i,k;

  while((c=getopt(argc,argv,"i:o:r:")) != -1)
    switch(c){
    case 'i':
      inputfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 'r':
      order=atoi(optarg);
      break;
    }
  float ref_form[order];
  float dir_form[order];
  float temp[order];
  float power;

  input=fopen(inputfile,"r");
  output=fopen(outputfile,"w");

  while(!feof(input)){
    //Reading in reflection coefficients
    for(i=0; i <= order; i++){
      if(i==0){
	fscanf(input,"%f",&power);
      }
      else{
	fscanf(input,"%f",&ref_form[i-1]);
      }
    }

    //Calculating direct form coefficients using Levinson-Durbin recursion (only need the equations that update the DF coefficients)
    for(i=0; i < order; i++){
      temp[i]=ref_form[i];

      for(k=0; k <= (i-1); k++){
	temp[k]=dir_form[k]+ref_form[i]*dir_form[i-k];
      }

      for(k=0; k < order; k++)
	dir_form[k]=temp[k];
    }

    //printing output
    fprintf(output,"%f ",power);
    for(i=0; i < order; i++)
      fprintf(output,"%f ",dir_form[i]);

    fprintf(output,"\n");
  }
  fclose(input);
  fclose(output);
  return 0;
}
