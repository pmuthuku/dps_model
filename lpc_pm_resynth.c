#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

int main(int argc,char **argv){
  FILE *lpc,*res,*out;
  int c,order;
  int res_length;
  int i,j,k,m;
  float channelno;
  int noframes;
  int samplerate=16000;
  char *lpccoefs,*resfile,*outfile;
  while((c=getopt(argc,argv,"f:r:p:o:n:l:")) != -1)
    switch(c){
    case 'f':
      lpccoefs=optarg;
      break;
    case 'r':
      resfile=optarg;
      break;
    case 'p':
      order=atoi(optarg);
      break;
    case 'o':
      outfile=optarg;
      break;
    case 'n':
      noframes=atoi(optarg);
      break;
    case 'l':
      res_length=atoi(optarg);
      break;
    }
  float coefs[noframes][order];
  float power[noframes];
  float resid[res_length];
  float sig[res_length];
  float time[noframes]; // float???
  //int framelength=(int)(res_length/noframes); // check if ceil or float would be a better option

  //Reading in lpc coefficients
  lpc=fopen(lpccoefs,"r");
  for(i=0; i < noframes; i++){
    for(j=0; j <= order+2; j++){
      if(j==0){
	fscanf(lpc,"%f",&time[i]);
	time[i]=time[i]*samplerate;
      }
      if(j==1){
	fscanf(lpc,"%f",&channelno); // Unused
      }
      if(j == 2){
	fscanf(lpc,"%f",&power[i]);
      }
      if(j > 2){
	fscanf(lpc,"%f",&coefs[i][j-3]);
      }
    }
  }
  fclose(lpc);

  //Reading in the residual
  res=fopen(resfile,"r");
  for(i=0;i < res_length; i++)
    fscanf(res,"%f",&resid[i]);
  fclose(res);

  //Synthesizing residual
  m=0; k=0;
  out=fopen(outfile,"w");
  for(i=0; i < res_length-1; i++){
    sig[i]=0;
    if(i >= time[k]){
      k++;
    }
    for(j=0; j < order; j++){
      if((i-j-1) >= 0){
	sig[i]=sig[i]+(sig[i-j-1]*coefs[k][j]);
      }
    }
    sig[i]=sig[i]+(sqrtf(power[k])*resid[i]);
    fprintf(out,"%f\n",sig[i]);
  }
  fclose(out);
  return 0;
}
