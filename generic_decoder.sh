#!/bin/bash
#Argument 1: Track file 2: outputfile
#Runs in voice_directory/test/mumble/ directory

$ESTDIR/bin/ch_track $1 -otype est_ascii | sed '1,/EST_Header_End/ d' > data_cding

filenm=`basename $1 .mcep`

perl -ane '
for($i=280; $i <= 293; $i++){
print "$F[$i] ";
}
print "\n";
' < data_cding > lpc_part

perl -ane '
for($i=21; $i <=278; $i++){
print "$F[$i] ";
}
print "\n";
' < data_cding > pca_part

#Will do this in a smarter way sometime
$ESTDIR/bin/ch_track $1 -otype est_ascii | sed '25,300 d' | perl -ane '
if($F[0] eq "NumChannels"){
  $F[1]=17;
}
@x=@F[0..19]; $num=@x;
for($i=0; $i < $num; $i++){
  if($i!=2){
    print "$x[$i] "; 
  }
}
print "\n";
' > speech_lpc_file

awk '{print $1}' < data_cding > tim1
paste tim1 lpc_part > stoc_part
paste tim1 pca_part > determ_part

/home/pmuthuku/code/dps_model/decoder_pca -i determ_part -o determf -s 256 -e ../../eigs.txt
/home/pmuthuku/code/dps_model/lpc_pm.sh stoc_part 12 stocf

#$DPS/normalizer determf out1
#$DPS/normalizer stocf out2

/home/pmuthuku/code/dps_model/upperlowercombiner -i determf -j stocf -o out -w 0.86
#awk '{print $1*3000}' < out > tesout
$ESTDIR/bin/ch_wave out -itype raw -istype ascii -scale 0.2 -f 16000 -otype riff -o res.wav
#/home/pmuthuku/fest_est/Feb12/flite/testsuite/lpc_resynth -res res.wav -order 16 speech_lpc_file temp.wav
/home/pmuthuku/fest_est/Feb12/flite/testsuite/lpc_resynth -res res.wav -order 16 ../../lpc/$filenm.lpc temp.wav
$ESTDIR/bin/ch_wave temp.wav -scale 3 -o $filenm.wav
rm -f data_cding lpc_part determ_part tim1 stoc_part stocf determf out tesout res.wav speech_lpc_file temp.wav pca_part
