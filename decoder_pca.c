#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>

//Fast algorithm to calculate LCM
int leastcommonmultiple(int k1,int k2){
  int x1=k1,x2=k2;
  while(x1!=x2){
    if(x1 < x2)
      x1=x1+k1;
    else if (x2 < x1)
      x2=x2+k2;
  }
  return x1; //or x2
}


int main(int argc,char **argv){
  FILE *fp1,*fp2;
  //FILE *fp3;
  int i;
  int c,j,k;
  int pm_locs[1000];
  float amplitude[1000];
  float framelength[1000];
  float excit[150000]={0};
  long int last=0;
  int length=0;
  int scale1;
  int nntemp;
  char *inputfile,*outputfile,*meanfile,*eigsfile;
  float p0,p1,p2,p3;
  float q0,q1,q2,q3;
  float t;
  float tens=0.2;
  float time;
  int lcm,m;
  int sizef=256;
  float samplingrate=16000;
  while((c=getopt(argc,argv,"i:o:s:e:")) != -1)
    switch(c){
    case 'i':
      inputfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 's':
      sizef=atoi(optarg);
      break;
    case 'e':
      eigsfile=optarg;
      break;
      /* case 'm':
      meanfile=optarg;
      break;*/
    }
  int no_vectors=sizef;
  float pca_wts[1000][no_vectors];
  float mean[sizef];
  float eig_str[sizef];
  float eig_vecs[no_vectors][sizef];
  float temp[sizef*800];
  float frame[sizef];

  //Reading in coding
  fp1=fopen(inputfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%f",&time);
    pm_locs[length]=time*samplingrate;
    fscanf(fp1,"%f",&framelength[length]);
    fscanf(fp1,"%f",&amplitude[length]);
    for(i=0; i < sizef; i++)
      fscanf(fp1,"%f",&pca_wts[length][i]);
    length++;
  }

  //Eigen vectors
  fp1=fopen(eigsfile,"r");
  for(i=0; i < no_vectors; i++){
    for(j=0; j < sizef; j++){
      /*if(i==0)
        fscanf(fp1,"%f",&eig_str[j]);
	else*/
        fscanf(fp1,"%f",&eig_vecs[i][j]);
    }
  }
  fclose(fp1);

  //Meanfile
  /*fp1=fopen(meanfile,"r");
  for(i=0; i < sizef; i++)
    fscanf(fp1,"%f",&mean[i]);
    fclose(fp1);*/

  //fp3=fopen("debug2","w");
  //Actual decoding part
  for(i=0; i < length-2; i++){
    //Initializing frame to zero
    for(j=0; j < sizef; j++)
      frame[j]=0;

    //"Un-pcaing"
    for(j=0; j < no_vectors; j++){
      for(k=0; k < sizef; k++){
	frame[k]=frame[k]+pca_wts[i][j]*eig_vecs[j][k];
      }
//fprintf(fp3,"%f ",frame[j]);//debug stuff
//frame[j]=frame[j]+mean[j];
    }
//fprintf(fp3,"\n");

    //Amplitude
    for(j=0; j < sizef; j++)
      frame[j]=amplitude[i]*frame[j];


    //Resampling
    if(framelength[i]==sizef){
      for(j=pm_locs[i]-(framelength[i]/2)/*verify*/,k=0; k < framelength[i]; j++,k++){
        excit[j]=excit[j]+frame[k];
        last=j;
      }
    }
    if(framelength[i]!=sizef){
      nntemp=framelength[i];
      lcm=leastcommonmultiple(nntemp,sizef);
      scale1=lcm/sizef;
      //Upsampling first
      for(j=0,k=0; j < lcm; j=j+scale1,k++){
        temp[j]=frame[k];
        //Boundary conditions
        p1=frame[k];
        if(k < sizef-1){
          p2=frame[k+1];
        }
        else{
          p2=0;
        }
        if(k > 0){
          p0=frame[k-1];
        }
        else{
          p0=0;
        }
        if(k >= (sizef-3)){
          p3=0;
        }
        else{
          p3=frame[k+2];
        }
        //Variables for the spline polynomial
        q0=p1;
        q1=tens*(p2-p0);
        q2=3*(p2-p1)-tens*(p3-p1)-2*tens*(p2-p0);
        q3=(-2)*(p2-p1)+tens*(p3-p1)+tens*(p2-p0);
        for(m=1; m <= (scale1-1); m++){
          //Catmull-Rom Spline interpolation to upsample signal
          t=((float)m)/((float)scale1);
          temp[j+m]=(q0+q1*t+q2*t*t+q3*t*t*t);
        }
      }
      //Now downsampling
      scale1=lcm/nntemp;
      for(j=(pm_locs[i]-(framelength[i]/2))/*verify*/,k=0; j < (pm_locs[i]+(framelength[i]/2)); j++,k=k+scale1){
	excit[j]=excit[j]+temp[k];
	last=j;
      }
    }

  }//End of decoding

  fp2=fopen(outputfile,"w");
  for(i=0; i<=last; i++)
    fprintf(fp2,"%f\n",excit[i]);
  fclose(fp2);
  return 0;
}
