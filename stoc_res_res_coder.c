#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>


int main(int argc,char **argv){
  FILE *fp1,*fp2;
  int c;
  int i;
  int count;
  char *inputfile,*outputfile,*pmfile;
  int pm_locs[1000];
  //  float time;
  int len_pm=0;
  //int samplingrate=16000;
  float sample; //Only sample is needed because we are only interested in power
  float peak=0;
  while((c=getopt(argc,argv,"i:o:p:")) != -1)
    switch(c){
    case 'i':
      inputfile=optarg;
      break;
    case 'o':
      outputfile=optarg;
      break;
    case 'p':
      pmfile=optarg;
      break;
    }

  //Reading in pitch marks
  fp1=fopen(pmfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%d",&pm_locs[len_pm++]);
  }
  fclose(fp1);

  //Reading in stochastic residual's residual
  fp1=fopen(inputfile,"r");
  fp2=fopen(outputfile,"w");
  i=0;
  count=0;
  while(!feof(fp1)){
    fscanf(fp1,"%f",&sample);
    if(fabsf(sample) > peak){
      peak=fabsf(sample);
    }
    if(count==pm_locs[i]){
      fprintf(fp2,"%f\n",peak);
      peak=0;
      i++;
    }
    count++;
  }
  fclose(fp1);
  fclose(fp2);
  return 0;
}
