#!/bin/bash
#To be run in the voice directory

max=`perl -ane '
@x=split(/=/,$F[0]);
print "$x[1] "' < etc/f0.params | awk '{print $3}'`

min=`perl -ane '                                                                
@x=split(/=/,$F[0]);
print "$x[1] "' < etc/f0.params | awk '{print $4}'`

cd lpc/
for i in *.res
do
FILENM=`basename $i .res`
echo $FILENM
$ESTDIR/bin/sigfilter $FILENM.res -otype raw -ostype ascii -o l4k -lpfilter 4000 -forder 55 -double

#Rewriting this to be more robust
#sed -n '9,$ p' < ../pm/$FILENM.pm | awk '{print int($1*16000)}' > data.pm
sed '1,/EST_Header_End/ d' ../pm/$FILENM.pm | awk '{print int($1*16000)}' > data.pm


/home/pmuthuku/code/dps_model/coder_nopca -i l4k -o l4k.cding -p data.pm -s 256 -m $max -n $min

cat l4k.cding >> ../temp_ps_traindata
rm l4k.cding l4k data.pm
done

awk '{$1="";$2="";$3="";print $0}' < ../temp_ps_traindata > ../ps_traindata
rm ../temp_ps_traindata
