#include <stdio.h>
#include <stdlib.h>

int main(int argc,char **argv){
  FILE *fp1;
  char *inputfile=argv[1];
  char *outputfile=argv[2];
  float max=0;
  float data[150000];
  int length=0;
  int i;
  fp1=fopen(inputfile,"r");
  while(!feof(fp1)){
    fscanf(fp1,"%f",&data[length++]);
    if(abs(data[length-1]) > max){
      max=abs(data[length-1]);
    }
  }
  fclose(fp1);
  fp1=fopen(outputfile,"w");
  for(i=0;i < length; i++){
    fprintf(fp1,"%f\n",(data[i]/max));
  }
  return 0;
}
