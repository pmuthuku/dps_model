#!/bin/bash

$ESTDIR/bin/sigfilter $1 -otype raw -ostype ascii -o u4k -hpfilter 4000 -forder 55 -double
$ESTDIR/bin/sigfilter $1 -otype raw -ostype ascii -o l4k -lpfilter 4000 -forder 55 -double
$ESTDIR/bin/sigfilter $1 -o u4k.wav -hpfilter 4000 -forder 55 -double

#Deterministic part
/home/pmuthuku/code/dps_model/coder_nopca -i l4k -o l4k.cding -p data.pm -s 256 -m 250 -n 50
/home/pmuthuku/code/dps_model/decoder_nopca -i l4k.cding -o out1 -s 256

/home/pmuthuku/code/dps_model/normalizer out1 test1
/home/pmuthuku/code/dps_model/normalizer u4k test2

/home/pmuthuku/code/dps_model/upperlowercombiner -i test2 -j test1 -o out -w 0.86

awk '{print $1*6000}' < out > corrout
$ESTDIR/bin/ch_wave corrout -itype raw -istype ascii -f 16000 -otype riff -o res1.wav

rm l4k.cding out1 out test1 test2
